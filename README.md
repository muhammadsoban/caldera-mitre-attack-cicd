git clone --recursive https://github.com/mitre/caldera.git
cd caldera
git remote add origin <https://github.com/mitre/caldera.git>
git push -u origin master
git add .gitlab-ci.yml
git commit -m "Add GitLab CI/CD configuration"
git push origin master
